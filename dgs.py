#!/usr/bin/env python3
from bs4 import BeautifulSoup
import requests
import time
import json
import os
from pathlib import Path

# <div id='mainnavigation'>
#    <div><a></div>
#    <div><a></div>

# generate index -> list of all videos + tags
# download all videos

# usage:
# 1. mirror_pages
# 2. mirror_videos # TODO: reimplement in this file
# [3. index_pages_from_fs] if pages are already mirrored
# 4. parse_menu
# 6. create_anki_deck

class Dgs:
    MIRROR_DIR = 'mirror'
    MIRROR_FILE = '{}/{{id}}.html'.format(MIRROR_DIR)
    DATA_DIR = 'data'
    DATA_FILE = '{}/{{name}}.dat'.format(DATA_DIR)
    def _get_data(self, name):
        try:
            return json.loads(open(Dgs.DATA_FILE.format(name=name)).read())
        except FileNotFoundError:
            return {}

    def _store_data(self, name, data):
        out = json.dumps(data, sort_keys=True, indent=4)
        with open(Dgs.DATA_FILE.format(name=name), 'w') as f:
            f.write(out)

    def __init__(self):
        # create directories
        os.makedirs(Dgs.DATA_DIR, exist_ok=True)
        # dictionary: id -> filename (relative to execution path)
        self.pages = self._get_data('pages')
        self.menu = self._get_data('menu')
        self.cards = self._get_data('cards')

        # content of html-pages
        self.mirror = {}

    def __del__(self):
        if hasattr(self, 'pages'):
            self._store_data('pages', self.pages)
        if hasattr(self, 'menu'):
            self._store_data('menu', self.menu)
        if hasattr(self, 'cards'):
            self._store_data('cards', self.cards)

    def index_pages_from_fs(self):
        for path in Path('.').glob(Dgs.MIRROR_FILE.format(id='*')):
            # id -> file_name
            self.pages[path.stem] = {'file': str(path)}

    def mirror_page(self, page_id, force_update=False):
        page_id = str(page_id)
        if page_id in self.pages:
            if force_update:
                # overwrite page
                del self.pages[page_id]
            else:
                # page already downloaded
                return
        cache_file = Dgs.MIRROR_FILE.format(id=page_id)
        url = 'https://gebaerdenlernen.de/index.php?article_id={}'.format(page_id)
        headers = {'user-agent': 'gebaerden-index/0.0.1'}
        r = requests.get(url, headers=headers, allow_redirects=False)
        if r.status_code == 200:
            # check if page exists (gebaerdenlernen.de returns 200 with index page instead of 404 errors)
            soup = BeautifulSoup(r.text, features='html.parser')
            if page_id != "1" and soup.find('div', id='contentheading').text.strip() == 'Home':
                print("{}: 404".format(page_id))
                self.pages[page_id] = {'http_status': 404}
                return
            self.pages[page_id] = {'http_status': r.status_code}
            os.makedirs(Dgs.MIRROR_DIR, exist_ok=True)
            with open(cache_file, 'w') as f:
                f.write(r.text)
                f.close()
            self.pages[page_id]['file'] = cache_file
            print("{}: Downloaded".format(page_id))
        elif r.status_code == 302:
            self.pages[page_id] = {'http_status': r.status_code}
            redirect = r.headers['Location'].split('=')[1]
            self.pages[page_id]['redirect'] = redirect
        else:
            raise ConnectionError

    def _get_page(self, page_id, redirect=False):
        page_id = str(page_id)
        if page_id not in self.pages:
            return
        
        # follow redirects
        dest_id = page_id
        if redirect:
            while 'redirect' in self.pages[dest_id]:
                dest_id = self.pages[dest_id]['redirect']

        if dest_id in self.mirror:
            return self.mirror[page_id]
        if 'file' not in self.pages[page_id]:
            return
        filename = self.pages[dest_id]['file']
        with open(filename) as f:
            soup = BeautifulSoup(f.read(), features='html.parser')
        if page_id != dest_id:
            self.mirror[dest_id] = soup
        return soup

    def mirror_pages(self):
        for i in range(256):
            print(i)
            self.mirror_page(i)

    def parse_menu_page(self, page_id):
        # get titles for pages and parent pages from menu
        # pages['page_id']['parent'] = 'parent_page_id'
        # pages['page_id']['title'] = 'title_text'
        page_id = str(page_id)
        if page_id in self.menu:
            # already parsed menu for this file
            return
        soup = self._get_page(page_id, redirect=True)
        if soup == None:
            return
        menu = soup.find('div', id='mainnavigation')
        last_lvl1_id = None
        lvl1_tag = ""
        last_lvl2_id = None
        lvl2_tag = ""
        for link in menu.find_all('div'):
            # get current page id and heading in menu
            cur_id = link.find('a').get('href').split('=')[1].strip()
            cur_title = link.find('a').text.strip().replace(' ', '_')
            if cur_id not in self.menu:
                self.menu[cur_id] = {'title': cur_title}
            if 'menu-lvl1' in link['class']:
                last_lvl1_id = cur_id
                lvl1_tag = cur_title
                self.menu[cur_id]['tags'] = [cur_title]
            elif 'menu-lvl2' in link['class']:
                last_lvl2_id = cur_id
                lvl2_tag = cur_title
                self.menu[cur_id]['tags'] = [lvl1_tag, cur_title]
            elif 'menu-lvl3' in link['class']:
                self.menu[cur_id]['tags'] = [lvl1_tag, lvl2_tag, cur_title]
            else:
                raise ValueError

    def parse_menu(self):
        for page in self.pages:
            self.parse_menu_page(page)
    
    def mirror_videos(self, page_id):
        # <a href="javascript:;" onclick="play('{file}');">{desc}</a>
        # download: https://gebaerdenlernen.de/vid/{file}.mp4
        page = self._get_page(page_id)
        if page == None:
            return
        os.makedirs('vid/', exist_ok=True) # todo: do this in __init__
        print(page_id, ": num", len(page.find_all('a', href='javascript:;')))
        for vid in page.find_all('a', href='javascript:;'):
            if vid.get('class') != None and 'close' in vid.get('class'):
                continue # skip link to close videos
            vid_name = vid.get('onclick').split("'")[1]
            filename = "vid/{file}.mp4".format(file=vid_name)
            if os.path.isfile(filename):
                continue
            url = "https://gebaerdenlernen.de/vid/{file}.mp4".format(file=vid_name)
            headers = {'user-agent': 'gebaerden-index/0.0.1'}
            r = requests.get(url, headers=headers, stream=True)
            print(url, '->', filename)
            with open(filename, 'wb') as fd:
                for chunk in r.iter_content(chunk_size=128):
                    fd.write(chunk)

    def add_cards(self, page_id):
        page_id = str(page_id)
        soup = self._get_page(page_id, redirect=False)
        if soup == None:
            return
        for voc_list in soup.find_all('div', class_='subwb'):
            cur_tags = self.menu[page_id]['tags'][:]
            # add title of voc list as tag
            # <p class="dictsection">Vokabular <b>{tag}</b></p>
            cur_tags.append(voc_list.find('b').text.strip().replace(' ', '_'))
            for voc in voc_list.find_all('a', href="javascript:;"):
                dgs_vid = voc['onclick'].split("'")[1].strip()
                de_translation = voc.text.strip()
                dgs_vid = "[sound:{}.mp4]".format(dgs_vid)
                if dgs_vid in self.cards:
                    # handle update -> same text on voc, other tags?
                    if self.cards[dgs_vid]['translation'] != de_translation:
                        print('Warning: card {} with different translation: {}, {}'.format(
                            dgs_vid, self.cards[dgs_vid]['translation'], de_translation))
                    for tag in cur_tags:
                        if tag not in self.cards[dgs_vid]['tags']:
                            print("Warning: card {} added tag {} to tags {}".format(
                                dgs_vid, tag, self.cards[dgs_vid]['tags']))
                            self.cards[dgs_vid]['tags'].append(tag)
                else:
                    self.cards[dgs_vid] = {
                            'translation': de_translation,
                            'tags': cur_tags
                    }

    def create_anki_deck(self):
        for page in self.menu:
            self.add_cards(page)
        deck = []
        for card in self.cards:
            deck.append("{}\t{}\t{}".format(
                card,
                self.cards[card]['translation'],
                " ".join(self.cards[card]['tags'])))
        csv = '\n'.join(deck)
        with open("dgs.txt", "w") as f:
            f.write(csv)

    def run(self):
        self.mirror_pages()
        self.parse_menu()
        self.create_anki_deck()

if __name__ == '__main__':
    import fire
    fire.Fire(Dgs())
