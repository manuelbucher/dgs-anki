# Kategorien auflisten
https://www.spreadthesign.com/de.de/search/by-category/

Datenstruktur:
categories = {
    "<Kategorie>": {
        "url": ""
    }
}

# Wörter von kategorien auflisten
words = {
    "wort": {
        "word_id"
        "vids": [
            vid_id1,
            vid_id2,
        ]
    }
}
https://www.spreadthesign.com/de.de/search/by-category/1/allgemeines/?p={page_number}
mit n seiten, jede seite hat maximal 50 Vokabeln, 

# video url herausbekommen für ein Wort
https://www.spreadthesign.com/de.de/word/{word_id}/v/{vid_var}/
mit n varianten, wobei n = min {n | url(n) = url(n+1)} (startet bei 0 zu zählen)

# video downloaden (id -> vid)
https://media.spreadthesign.com/video/mp4/9/{vid_id}.mp4

