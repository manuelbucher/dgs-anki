## Dgs Anki-Karten

```bash
python3 -m venv venv
. ./venv/bin/activate
pip install fire beautifulsoup4 requests
./dgs.py run
```

## Ähnliche Projekte:

* https://signdict.org/
  * https://ankiweb.net/shared/info/1439024119
  * https://www.guessr.morr.cc/anki-dgs/
* https://gebaerdenlernen.de/
* https://www.spreadthesign.com/
